<?php

namespace App\Helpers;

class SearchFilter extends QueryFilters
{
    public function name($value){
        if (trim($value) !== '')
        $this->builder = $this->builder->where('name', 'LIKE' ,"%$value%");
    }

    public function surname($value){
        if (trim($value) !== '')
        $this->builder = $this->builder->where('surname', 'LIKE' , "%$value%");
    }

    public function country($value){
        if (trim($value) !== '')
        $this->builder = $this->builder->where('country', 'LIKE' , "%$value%");
    }

    public function phone($value){
        if (trim($value) !== '')
        $this->builder = $this->builder->where('phone', 'LIKE' , "%$value%");
    }

}