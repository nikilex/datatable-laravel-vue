<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Helpers\SearchFilter;

class ProfileController extends Controller
{
    /**
     * @var Profile
     */
    private $profile;
    /**
     * ProfileController constructor.
     * @param Profile $profile
     */
    public function __construct(Profile $profile)
    {
    	$this->profile = $profile;
    }
    /**
     * return paginated records of profiles
     * 
     * @return \Illuminate\Http\JsonResponse
     */

    public function index(Request $request)
    {
        $profiles = (new SearchFilter($this->profile, $request))->apply()->paginate(10);
        
    	return response()->json($profiles);
    }

}
